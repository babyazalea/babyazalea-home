import React from "react";

import "./Prologue.scss";

const Prologue = () => {
  return (
    <div className="prologue">
      <p>실패의 완성은</p>
      <p>완전한 시작.</p>
    </div>
  );
};

export default Prologue;
