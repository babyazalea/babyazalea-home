import React from "react";
import { graphql } from "gatsby";

import Layout from "../components/layout/Layout";
import "./blog-post.scss";

export const query = graphql`
  query PostQuery($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      frontmatter {
        title
        date(formatString: "YYYY-MM-DD")
        category
      }
      fields {
        slug
      }
    }
  }
`;

const BlogPostTemplate = ({ data }) => {
  const post = data.markdownRemark;

  return (
    <Layout customClassName="post-template">
      <div className="blog-post-caption">
        <h1>{post.frontmatter.title}</h1>
        <span>{post.frontmatter.date}</span>
      </div>
      <br />
      <br />
      <section
        dangerouslySetInnerHTML={{ __html: post.html }}
        itemProp="articleBody"
      />
    </Layout>
  );
};

export default BlogPostTemplate;
